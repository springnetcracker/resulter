package com.resulter.bll;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by denis on 28.04.16.
 */
public class LoginService {
    private static final Logger logger = Logger.getLogger(LoginService.class);
    //public String cookie = null;
    Header[] cookie = null;

    public String login() throws IOException {
        String host = "localhost";
        int port = 8080;
        String login = "RESULTER";
        String password = "resulter";
        HttpClient httpClient = new DefaultHttpClient();
        URI manoLoginUri = null;
        try {
            manoLoginUri = new URIBuilder().setScheme("http").
                    setHost(host).setPort(port).setPath("/ReportsGenerator/j_spring_security_check").build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("login", login));
        params.add(new BasicNameValuePair("password", password));

        final HttpPost httpPost = new HttpPost(manoLoginUri);
        httpPost.setHeader(HttpHeaders.ACCEPT, "text/html");
        httpPost.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = null;

        try {
            response = httpClient.execute(httpPost);
            cookie = (Header[]) response.getHeaders("Set-Cookie");
        } catch (Exception e){
            logger.error("Error in login",e);
            e.printStackTrace();
            throw  e;
        }
        return cookie[0].getValue();
    }

}
