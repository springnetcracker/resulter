package com.resulter.bll;

import com.resulter.jdbcconnections.MySQL;
import com.resulter.jdbcconnections.Oracle;
import com.resulter.jdbcconnections.PostgreSql;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by denis on 15.04.16.
 */
public class Report {
    private List<String> incomingList=null;
    private String typeDB;
    private String query;
    private String connectionURL;
    private Connection conn=null;
    private int reportId;
    private String username;
    private String pass;

    private static final Logger logger = Logger.getLogger(Report.class);

    public Report(){

    }
    public Report(byte[] inputData){
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new ByteArrayInputStream(inputData));
            incomingList = (ArrayList<String>) ois.readObject();
        } catch (IOException  | ClassNotFoundException e ) {
            e.printStackTrace();
        } finally {
            try {
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            typeDB = incomingList.get(3).toLowerCase();
            connectionURL = incomingList.get(2);
            query = incomingList.get(0);
            reportId = Integer.parseInt(incomingList.get(1));
            username = incomingList.get(4);
            pass = incomingList.get(5);
        } catch (Exception e){
            e.printStackTrace();
        }


    }


    public Connection getJDBCConnection(){
        if (!incomingList.isEmpty()){
            switch (typeDB){
                case "mysql":
                    conn=new MySQL().getSqlConnection(connectionURL, username, pass);
                    break;
                case "oracle":
                    conn=new Oracle().getSqlConnection(connectionURL, username, pass);
                    break;
                case "postgresql":
                    conn=new PostgreSql().getSqlConnection(connectionURL,username, pass);
            }
        }
        return conn;
    }


    public JSONObject createReport(){
        List<String[]> resultOfQuery = getData(getJDBCConnection(), query);
        JSONObject obj = new JSONObject();
        obj.put("rows", resultOfQuery);
        return obj;
    }

    public void sendResult(String cookie, int id){
        JSONObject result=createReport();
        try{
            URL url = new URL("http://localhost:8080/ReportsGenerator/core/report/upd/"+id);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("PUT");
            conn.setDoOutput( true );
            conn.setInstanceFollowRedirects( false );
            conn.setRequestProperty( "Content-Type", "application/json");
            conn.setRequestProperty("Cookie", cookie);
            conn.setRequestProperty( "charset", "utf-8");
            //conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
            conn.setUseCaches( false );
            try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                wr.write( result.toString().getBytes("utf-8") );
            }

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
        } catch (IOException e){
            logger.error("Error in send result", e);
            e.printStackTrace();
        }
    }

    public List<String[]> getData(Connection connectionSQL, String userQuery) {
        List<String[]> listOfColumns=null;
        Statement stmt = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        try {

            stmt = connectionSQL.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(userQuery);
            int size = 0;
            rs.last();
            size = rs.getRow();
            rs.beforeFirst();

            if (size>0) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int numberOfColumns = rsmd.getColumnCount();
                String[] row = new String[numberOfColumns]; //для получение массива из строки
                listOfColumns = new ArrayList<>(); //для полного select

                //запись заголовков
                for (int i = 0; i < numberOfColumns; i++) {
                    row[i] = rsmd.getColumnLabel(i + 1);
                }
                listOfColumns.add(row);

                while (rs.next()) {
                    row = new String[numberOfColumns];
                    for (int i = 0; i < numberOfColumns; i++) {
                        if (!(rs.getString(i + 1)==null)) {
                            row[i] = rs.getString(i + 1);
                        } else {
                            row[i] = "";
                        }
                    }
                    listOfColumns.add(row);
                }


            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            logger.error("Error:", ex);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { rs.close(); } catch (Exception ignore) {}
            try { stmt.close(); } catch (Exception ignore) {}
            try { connectionSQL.close(); } catch (Exception ignore) {}
        }
        return listOfColumns;
    }

    public int getReportId() {
        return reportId; //return id of report for upd it in db
    }
}
