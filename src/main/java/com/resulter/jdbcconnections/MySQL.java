package com.resulter.jdbcconnections;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by denis on 15.04.16.
 */
public class MySQL implements  UserDB{
    private static final Logger logger = Logger.getLogger(MySQL.class);
    @Override
    public Connection getSqlConnection(String connectionURL, String username, String password) {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(connectionURL+"?user="+username+"&password="+password);
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("ERROR in MySQL", e);
        }
        return conn;
    }
}
