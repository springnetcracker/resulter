package com.resulter.jdbcconnections;

import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;

/**
 * Created by denis on 15.04.16.
 */
public class Oracle implements UserDB{

    private static final Logger logger = Logger.getLogger(Oracle.class);
    @Override
    public Connection getSqlConnection(String connectionURL, String username, String password) {

        Connection conn = null;
        try {
            Locale.setDefault(Locale.UK);
            try {

                try {
                    Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
                } catch (ClassNotFoundException e) {
                    logger.error("Where is your Oracle JDBC Driver?", e);
                    e.printStackTrace();
                }
                conn = DriverManager.getConnection(connectionURL, username, password);// URL коннекции

            } catch (Exception e){
                logger.error("ERROR", e);
                e.printStackTrace();
                throw e;
            }

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("error in getting connetion ORACLE", e);
        }
        return conn;
    }
}
