package com.resulter.jdbcconnections;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by denis on 03.05.16.
 */
public class PostgreSql implements UserDB {
    private static final Logger logger = Logger.getLogger(PostgreSql.class);

    public java.sql.Connection getSqlConnection(String connectionURL, String username, String password) {
        java.sql.Connection conn = null;

        try {
            Class.forName("org.postgresql.Driver").newInstance();
            conn = DriverManager.getConnection(connectionURL,username,password);

        } catch (Exception ex) {
            //System.out.println("SQLException: " + ex.getMessage());
            //System.out.println("SQLState: " + ex.getSQLState());
            //System.out.println("VendorError: " + ex.getErrorCode());
            logger.error("Error in postgre", ex);
        }

        return conn;
    }
/*
    public String stringAccess(Connection connection) {
        String hostIp = connection.getHost();
        int port = connection.getPort();
        String nameDB = connection.getdBName();
        String stringAccess = "jdbc:postgresql://"+hostIp+":"+port+"/"+nameDB;
        return stringAccess;
    }*/
}
