package com.resulter.jdbcconnections;

import java.sql.Connection;

/**
 * Created by denis on 15.04.16.
 */
public interface UserDB {

    Connection getSqlConnection(String connectionURL, String username, String password);
}
