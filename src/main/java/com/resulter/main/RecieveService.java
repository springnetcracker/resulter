package com.resulter.main;

import com.rabbitmq.client.*;
import com.resulter.bll.LoginService;
import com.resulter.bll.Report;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by denis on 15.04.16.
 */
public class RecieveService {

    private static final Logger logger = Logger.getLogger(RecieveService.class);
    //private static final Logger logger = Logger.getRootLogger();
    private final static String QUEUE_NAME = "queue1";

    public static  void main (String[] args) throws java.io.IOException, java.lang.InterruptedException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = null;
        try {
            connection = factory.newConnection();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        logger.info("I'M started!");


        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                Report report = new Report(body);
                LoginService ls = new LoginService();
                try {
                    logger.setLevel(Level.INFO);
                    logger.info("The data obtained from the core ");
                    String cookie=  ls.login();
                    logger.info("Authentication successfully passed. Cookie="+cookie);
                    report.sendResult(cookie, report.getReportId());
                    logger.info("JSON successfully sent ");
                } catch (Exception e){
                    e.printStackTrace();
                    logger.error("Something went wrong ", e);
                    throw e;
                }
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
